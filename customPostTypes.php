<?php
/*
	
    Plugin Name: customPostTypes
    Description: Blumers custom post types
    Author: Pursuit Technology
    Version: 0.1.1

*/

class WPS_Setup {
    function __construct() {
        add_action( 'init', array($this, 'register_post_types'));
    }

    /**
     * Registers the custom post types for screenings and events.
     * @return void
     */
	public function register_post_types() {    
        $post_type_args = array(
            'labels' => array(
                'name' => 'Team',
                'singular_name' => 'Team',
                'add_new' =>  'Add new team member',
                'new_item' => 'New team member',
                'add_new_item' => 'Add new team member',
                'edit_item' => 'Edit team member'
            ),
            'public' => true,
            'has_archive' => true,
            'show_in_menu'  => true,
            'show_in_admin_bar' => true,
            'supports' => array(
                'title',
                'editor',
                // 'excerpt',
                'thumbnail',
                // 'comments'
            ),
            // 'taxonomies' => array('category','post_tag'),
            'rewrite' => array( 
                // 'slug' => $wp_theatre->production_permalink->get_base(), 
                'with_front' => false, 
                'feeds' => true 
            ),
        );

        register_post_type( 'wp_team_member', $post_type_args );
    }
}

$wp_screenings = new WPS_Setup();

?>